﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InAgSolucion
{
    public class ProductosCongelados : ProductoGeneral
    {
        public TemperaturaIdeal temperaturaIdeal { get; set; }
        public CongeladoAgua congeladoAgua { get; set; }
        public CongeladoAire congeladoAire { get; set; }
        public CongeladoNitrogeno congeladoNitrogeno { get; set; }


    }
}
