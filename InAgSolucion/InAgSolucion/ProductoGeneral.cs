﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InAgSolucion
{
    public class ProductoGeneral
    {
        public Fecha fechaCaducidad { get; set; }
        public string lote { get; set; }
        public Fecha fechaEnvasado { get; set; }
        public string paisOrigen { get; set; }

    }
}
